package main

import (
	"context"
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/guardduty"
	"github.com/aws/aws-sdk-go-v2/service/guardduty/types"
	"github.com/jedib0t/go-pretty/v6/table"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func init() {
	viper.SetEnvPrefix("gdr")
	viper.BindEnv("regions")
	viper.BindEnv("log_level")
	viper.BindEnv("show_archived")
	viper.BindEnv("severity")

	viper.SetDefault("regions", []string{"eu-central-1", "eu-west-1"})
	viper.SetDefault("log_level", log.WarnLevel)
	viper.SetDefault("show_archived", "false")
	viper.SetDefault("severity", 7.0)

	log.SetFormatter(&log.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})
	log.SetLevel(log.Level(viper.GetInt("log_level")))
}

func main() {
	var regions []string
	var showArchived bool
	var severity int64

	err := viper.UnmarshalKey("regions", &regions)
	if err != nil {
		log.Fatal(err)
	}

	err = viper.UnmarshalKey("show_archived", &showArchived)
	if err != nil {
		log.Fatal(err)
	}

	err = viper.UnmarshalKey("severity", &severity)
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		log.Fatal(err)
	}

	for _, region := range regions {
		fmt.Println(region)
		cfg.Region = region
		displayFindings(cfg, showArchived, severity)
	}

}

func displayFindings(cfg aws.Config, showArchived bool, severity int64) {
	client := guardduty.NewFromConfig(cfg)

	detectors := determineDetectors(client)
	log.Debug("Found detectors:", detectors)

	if len(detectors) == 0 {
		log.Warn("No detectors found in region ", cfg.Region)
	}

	for _, detector := range detectors {
		findingIds := getFindings(client, detector, showArchived, severity)
		out, err := client.GetFindings(context.TODO(), &guardduty.GetFindingsInput{DetectorId: &detector, FindingIds: findingIds})
		if err != nil {
			log.Warn("Could not determine finding details.")
		}

		printTable(out.Findings)
	}
}

func printTable(findings []types.Finding) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"Severity", "AccountId", "Title", "FindingId"})
	for _, fnd := range findings {
		t.AppendRow([]interface{}{fnd.Severity, *fnd.AccountId, *fnd.Title, *fnd.Id})
	}
	t.Render()

}

func determineDetectors(client *guardduty.Client) []string {

	out, err := client.ListDetectors(context.TODO(), &guardduty.ListDetectorsInput{})

	if err != nil {
		log.Fatal("Could not determine Detectors: ", err)
	}

	return out.DetectorIds

}

func getFindings(client *guardduty.Client, detector string, showArchived bool, severity int64) []string {

	var archivedCondition []string

	if showArchived {
		archivedCondition = []string{"true"}
	} else {
		archivedCondition = []string{"false"}
	}

	findings, err := client.ListFindings(
		context.TODO(),
		&guardduty.ListFindingsInput{
			DetectorId: &detector, FindingCriteria: &types.FindingCriteria{
				Criterion: map[string]types.Condition{
					"severity":         {GreaterThanOrEqual: severity},
					"service.archived": {Equals: archivedCondition},
				}},
		})
	if err != nil {
		log.Warn("Could not get findings for", detector, "due to", err)
	}

	return findings.FindingIds
}
